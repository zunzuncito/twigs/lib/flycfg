
import fs from 'fs'
import path from 'path'

import YAML from 'yaml'


import Project from './project/parser.mjs'

export default class FlyCfg {
  static async project(app_path) {
    const zunzun_json_path = Project.check(app_path);
    
    // -- disabled
    const preconf_path = path.resolve(app_path, 'preconf.mjs');
    // --------------

    let app_cfg = undefined;
    try {
      app_cfg = YAML.parse(fs.readFileSync(zunzun_json_path, 'utf8'));
      app_cfg.path = app_path;
    } catch (e) {
      console.error(e.stack);
      process.exit();
    }

    const layers = Project.parse(app_path);
    await Project.preconf(layers, app_cfg, {});

    app_cfg.services = [];
    for (let srvgroup of layers) {
      for (let srv of srvgroup) {
        const nsrv = {
          name: srv.name,
          config: srv.config
        };
        if (srv.disabled) nsrv.disabled = true;
        app_cfg.services.push(nsrv);
      }
    }

    return { layers: layers, app_cfg: app_cfg };

  }
}
