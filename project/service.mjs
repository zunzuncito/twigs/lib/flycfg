
import fs from 'fs'
import path from 'path'

import YAML from 'yaml'

import ObjectUtil from 'zunzun/flyutil/object.mjs'
import FlyModule from 'zunzun/flymodule/index.mjs'

export default class ServiceParser {
  static parse(srv_path) {
    const srvcfg_path = path.resolve(srv_path, ".srvconfig.yaml");

    const srv_obj = {
      name: srv_path.split("/").pop(),
      path: srv_path
    }
    try {
      srv_obj.config = fs.existsSync(srvcfg_path) ?
        YAML.parse(fs.readFileSync(srvcfg_path, 'utf8')) : {};
    } catch (e) {
      console.log(`Error parsing configuration for service '${srv_path}':`);
      console.error(e.stack);
    }

    return srv_obj;
  }

  static async preconf(srv_obj, cfg_override, flycfg) {
    const preconf_path = path.resolve(srv_obj.path, "preconf.mjs");
    if (fs.existsSync(preconf_path)) {
      let this_module = await FlyModule.load(srv_obj.path, "preconf.mjs");
      if (!this_module) return undefined;

      let resulting_config = undefined;
      if (this_module.default.run) {
        resulting_config = await (this_module.default.run(flycfg, srv_obj.config));
      }

      srv_obj.config = resulting_config;

    }
    srv_obj.config = ObjectUtil.force_add(srv_obj.config, cfg_override);

    return srv_obj;
  }


}
