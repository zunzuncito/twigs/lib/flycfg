
import fs from 'fs'
import path from 'path'

import PathUtil from 'zunzun/flyutil/path.mjs'

import Service from './service.mjs'

export default class ProjectParser {
  static check(app_path) {
    const zunzun_json_path = PathUtil.resolve_by_type(path.resolve(app_path, '.zunzun'), 'yaml');
    if (!fs.existsSync(zunzun_json_path)) {
      throw new Error(`Project configuration file '${zunzun_json_path}' does not exist!`);
    }
    const zunzun_twigs_srv_path = path.resolve(app_path, 'twigs/srv');
    if (!fs.existsSync(zunzun_twigs_srv_path)) {
      throw new Error(`Service directory '${zunzun_twigs_srv_path}' does not exist!`);
    }
    return zunzun_json_path;
  }

  static parse_mixed(app_path) {
    const mixed_services = [];

    const srvs_path = path.resolve(app_path, 'twigs/srv');
    const srvs = fs.readdirSync(srvs_path);
    for (let srv of srvs) {
      const srv_path = path.resolve(srvs_path, srv);
      mixed_services.push(Service.parse(srv_path));
    }
    mixed_services.sort((a, b) => a.name.localeCompare(b.name));

    return mixed_services;
  }

  static validate(mixed_services) {
    function service_exists(srv_name) {
      for (let s = 0; s < mixed_services.length; s++) {
        if (mixed_services[s].name == srv_name) return true;
      }
      return false;
    }

    for (let srv of mixed_services) {
      if (srv.config.start && srv.config.start.after) {
        for (let sa_srv of srv.config.start.after) {
          if (
            !service_exists(sa_srv) && !(
              typeof sa_srv == "string" &&
              (sa_srv == "*" || sa_srv.toLowerCase() == "all")
            )
          ) {
            console.error(new Error(`'srv/${srv.name}' waiting for 'srv/${sa_srv}' which does not exist!`));
            console.log("Exiting to prevent infinite loop!");
            process.exit();
          }
        }
      }
    }
  }

  static layers(mixed_services) {
    const srv_layers = [];

    while (mixed_services.length > 0) {
      const srv_group = [];
      const rm_indexes = [];

      for (let s = 0; s < mixed_services.length; s++) {
        let srv = mixed_services[s];
        if (srv.config.start && srv.config.start.after) {
          if (
            typeof srv.config.start.after == "string" &&
            (srv.config.start.after == "*" || srv.config.start.after.toLowerCase() == "all")
          ) {
            let start_after = [];
            for (let qsrv of mixed_services) {
              if (
                qsrv.name !== srv.name &&
                !(
                  qsrv.config.start && 
                  typeof qsrv.config.start.after == "string" &&
                  (qsrv.config.start.after == "*" || qsrv.config.start.after.toLowerCase() == "all")
                )
              ) {
                start_after.push(qsrv.name);
              }
            }

            for (let i = start_after.length-1; i > -1; i--) {
              const sa_name = start_after[i];
              let sloaded = false;

              srv_layers_loop:
              for (let qgroup of srv_layers) {
                for (let qsrv of qgroup) {
                  if (qsrv.name === sa_name) {
                    sloaded = true;
                    break srv_layers_loop;
                  }
                }
              }

              if (sloaded) start_after.splice(i, 1);
            }

            if (start_after.length == 0) {
              srv_group.push(srv);
              rm_indexes.push(s);
            }
          } else {
            let start_after = [...srv.config.start.after];

            for (let i = start_after.length-1; i > -1; i--) {
              const sa_name = start_after[i];
              let sloaded = false;

              srv_layers_loop:
              for (let qgroup of srv_layers) {
                for (let qsrv of qgroup) {
                  if (qsrv.name === sa_name) {
                    sloaded = true;
                    break srv_layers_loop;
                  }
                }
              }

              if (sloaded) start_after.splice(i, 1);
            }

            if (start_after.length == 0) {
              srv_group.push(srv);
              rm_indexes.push(s);
            }
          }
        } else {
          srv_group.push(srv);
          rm_indexes.push(s);
        }
      }

      for (let i = rm_indexes.length-1; i > -1; i--) {
        mixed_services.splice(rm_indexes[i], 1)
      }

      srv_layers.push(srv_group);
    }

    return srv_layers;
  }

  static parse(app_path) {
    const mixed = ProjectParser.parse_mixed(app_path);
    ProjectParser.validate(mixed);
    return ProjectParser.layers(mixed);
  }

  static async preconf(layers, app_cfg, flycfg) {
    try {
      for (let qgroup of layers) {
        for (let qsrv of qgroup) {
          let cfg_override = {};
          if (Array.isArray(app_cfg.services)) {
            for (let zzcfg_srv of app_cfg.services) {
              if (zzcfg_srv.name === qsrv.name) {
                if (zzcfg_srv.disabled) qsrv.disabled = true;
                cfg_override = zzcfg_srv.config;
              }
            }
          }
          await Service.preconf(qsrv, cfg_override, flycfg);
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }
}
